const inputPriceField = document.querySelector('.input-price');

inputPriceField.addEventListener('blur', () => {
    const priceDiv = document.createElement('div');
    const priceSpan = document.createElement('span');
    const deleteButton = document.createElement('button');
    const checkErrorMessage = document.querySelector('.error');
    const inputValue = Number(inputPriceField.value);


    if(!isNaN(Number(inputPriceField.value)) && inputPriceField.value !== '' && inputValue > 0){
        priceDiv.classList.add('price');
        priceSpan.classList.add('text');
        deleteButton.classList.add('delete-button');

        inputPriceField.classList.remove('redError');

        priceSpan.innerText = `Текущая цена: ${inputPriceField.value}`;
        deleteButton.innerText = 'X';

        if (checkErrorMessage) {
            checkErrorMessage.remove()
        }

        deleteButton.addEventListener('click', () => {
            priceDiv.remove();
            inputPriceField.value = '';
        });

        priceDiv.appendChild(priceSpan);
        priceDiv.appendChild(deleteButton);

        document.body.appendChild(priceDiv);

    } else {

        const propertyValue = document.createElement('span');

        inputPriceField.classList.add('redError');

        if (checkErrorMessage) {
            checkErrorMessage.remove()
        }

        propertyValue.classList.add('error');
        propertyValue.innerText = 'Enter property value';

        priceDiv.appendChild(propertyValue);
        document.body.appendChild(priceDiv);
    }


});